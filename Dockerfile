FROM python:3-bullseye

ENV PYTHONPATH "${PYTHONPATH}:/app"
ENV PIP_DISABLE_PIP_VERSION_CHECK 1

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY bot /app/bot

CMD ["python", "/app/bot/main.py"]
