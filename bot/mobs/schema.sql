DROP TABLE IF EXISTS epicos;

CREATE TABLE epicos (
       nombre TEXT UNIQUE NOT NULL,
       fecha_salida TIMESTAMP NOT NULL,
);
