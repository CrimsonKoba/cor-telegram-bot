#!/usr/bin/env python3
import os
import time
import sqlite3 as sql
from datetime import datetime, timedelta

import pytz


class MobRes:
    def __init__(self, mob: str):
        self._set_utc()
        self.db_con = self._db_con()
        self.res_date = datetime.now()
        self.r_time = self.select_one(mob)

    def __del__(self):
        if self.db_con:
            self.db_con.close()

    def _set_utc(self):
        if 'TZ' not in os.environ or os.environ['TZ'] != 'UTC':
            os.environ['TZ'] = 'UTC'
            time.tzset()

    def _db_con(self):
        try:
            con = sql.connect(
                os.path.dirname(os.path.realpath(__file__)) + '/mobs.db',
                detect_types=sql.PARSE_DECLTYPES)
        except sql.Error as error:
            print('Error al conectar a la base de datos ', error)

        return con

    def _db_list(self):
            cur = self.db_con.cursor()
            db_select = 'SELECT nombre, fecha_salida FROM epicos'
            cur.execute(db_select)
            records = cur.fetchall()
            return records

    def _db_update_date(self, re_date, name):
        cur = self.db_con.cursor()
        sql = 'UPDATE epicos SET fecha_salida = ? WHERE nombre = ?'
        data = (re_date, name)
        cur.execute(sql, data)
        self.db_con.commit()

    def countdown(self):
        present = datetime.now()
        while self.res_date < present:
            self.res_date += timedelta(hours=109)

        return self.res_date - present

    def countdown_text(self, respawn):
        days = respawn.days
        hours = respawn.seconds // 3600
        minutes = (respawn.seconds // 60) % 60
        return f'Sale en {days} dias, {hours} horas y {minutes} minutos'

    def evendim(self):
        self.res_date = self._db_list()[0][1]
        cd = self.countdown()

        self._db_update_date(self.res_date, 'ev')

        return self.countdown_text(cd)

    def thorkul(self):
        self.res_date = self._db_list()[1][1]
        cd = self.countdown()

        self._db_update_date(self.res_date, 'tk')

        return self.countdown_text(cd)

    def daen_rha(self):
        self.res_date = self._db_list()[2][1]
        cd = self.countdown()

        self._db_update_date(self.res_date, 'dr')

        return self.countdown_text(cd)

    def select_one(self, e):
        if e == 'ev':
            this = self.evendim()
        elif e == 'tk':
            this = self.thorkul()
        elif e == 'dr':
            this = self.daen_rha()
        else:
            this = None
            print('Ese epico no existe.')
        return this

    def as_timezone(self, tz):
        set_tz = pytz.timezone

        if tz == 'arg':
            new_tz = set_tz('America/Buenos_Aires')
        elif tz == 'brz':
            new_tz = set_tz('America/Sao_Paulo')
        elif tz == 'chl':
            new_tz = set_tz('America/Sao_Paulo')
        elif tz == 'esp':
            new_tz = set_tz('Europe/Madrid')
        elif tz == 'mex':
            new_tz = set_tz('America/Monterrey')
        else:
            new_tz = set_tz('UTC')

        return self.res_date.replace(tzinfo=pytz.utc).astimezone(new_tz)
