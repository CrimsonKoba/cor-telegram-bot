from . import MobRes


class WhichEpico:
    MR = None


def select(nombre: str):
    if nombre == "evendim":
        WhichEpico.MR = MobRes("ev")
    elif nombre == "thorkul":
        WhichEpico.MR = MobRes("tk")
    elif nombre == "daen":
        WhichEpico.MR = MobRes("dr")
    else:
        return "La criatura no existe."
