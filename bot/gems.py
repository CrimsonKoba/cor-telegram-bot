#!/usr/bin/env python3
import re
from urllib.request import urlopen
from bs4 import BeautifulSoup as bs


class GemsInfo:
    def __init__(self):
        self.url = 'https://www.championsofregnum.com/index.php?l=0&sec=3'
        self.page = self.read_page()

    def read_page(self):
        page_source = urlopen(self.url)
        return page_source.read()

    def count_gems(self, kingdom=None):
        soup = bs(self.page, 'lxml')
        reino = soup.find('div', string=re.compile(kingdom))
        g_img = reino.find_next_sibling().find_all('img')

        count = 0
        gem_r = 'gem_1.png'
        gem_b = 'gem_2.png'
        gem_g = 'gem_3.png'

        for i in g_img:
            i = str(i['src'])
            if i.endswith(gem_r) or i.endswith(gem_b) or i.endswith(gem_g):
                count += 1

        if count == 0:
            count = 'Devastado'

        return count
