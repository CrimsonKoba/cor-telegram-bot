#!/usr/bin/env python3
import os
import sys
import logging

from textwrap import dedent
from datetime import datetime

import emoji
from aiogram import Bot, Dispatcher, executor
from aiogram.dispatcher.filters import Text
from aiogram.types import (
    Message,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
    CallbackQuery,
)

import mobs
import gems


# Configure logging
logging.basicConfig(level=logging.INFO)

# Read token from env if exists or from .token file if not
if os.getenv("TELEGRAM_TOKEN"):
    token = os.getenv("TELEGRAM_TOKEN")
else:
    with open(".token", "r") as file:
        token = file.readline().splitlines()[0]

# Initialize bot
if token:
    bot = Bot(token=token)
    dp = Dispatcher(bot)
else:
    logging.error("No token file")
    sys.exit()


@dp.message_handler(commands="start")
async def send_welcome(message: Message):
    text = """
    Bienvenido al bot de *Champions of Regnum*
    Usa el comando _/help_ para obtener ayuda."""
    await message.answer(dedent(text), parse_mode="Markdown")


@dp.message_handler(commands="help")
async def help_msg(message: Message):
    text = """
    Para obtener una imagen del estado actual de los reinos
    solo escribe la palabra *mapa*.

    Para conocer el tiempo restante para la aparecion de los
    *epicos* de cada reino basta con escribir su nombre;
    _Evendim_, _Thorkul_ o _Daen_.

    Para el conteo de gemas de los reinos escribe *gemas*."""
    await message.answer(dedent(text), parse_mode="Markdown")


@dp.message_handler(Text(equals=["map", "mapa"], ignore_case=True))
async def ro_map(message: Message):
    timestamp = datetime.now().isoformat()
    img = "https://www.championsofregnum.com/ranking/data/ra/gen_map.jpg"
    url = f"{img}?ts={timestamp}"
    await message.answer_photo(url)


@dp.message_handler(Text(equals=["evendim", "thorkul", "daen"], ignore_case=True))
async def epicos(message: Message):
    mobs.select(message.text.lower())
    kb_markup = InlineKeyboardMarkup()
    row_button = InlineKeyboardButton("Mostrar hora y fecha", callback_data="show_dt")
    kb_markup.row(row_button)

    await message.reply(mobs.WhichEpico.MR.r_time, reply_markup=kb_markup)


# TODO
@dp.message_handler(commands="notify")
async def notify_mobs(message: Message):
    message_id = message.chat.id
    await message.reply(str(message_id))


@dp.callback_query_handler(text="show_dt")
async def ikb_response_respawn_epicos(query: CallbackQuery):
    kb_markup = InlineKeyboardMarkup()

    opts1 = (
        ("Argentina", "arg"),
        ("Brazil", "brz"),
        ("Chile", "chl"),
    )
    opts2 = (
        ("España", "esp"),
        ("Mexico", "mex"),
    )

    r1_btns = (InlineKeyboardButton(text, callback_data=data) for text, data in opts1)
    r2_btns = (InlineKeyboardButton(text, callback_data=data) for text, data in opts2)
    kb_markup.row(*r1_btns)
    kb_markup.add(*r2_btns)

    await query.message.edit_text(mobs.WhichEpico.MR.r_time, reply_markup=kb_markup)


@dp.callback_query_handler(text=["arg", "brz", "chl", "esp", "mex"])
async def ikb_response_respawn_local_td(query: CallbackQuery):
    kb_markup = InlineKeyboardMarkup()
    kb_markup.row(InlineKeyboardButton("...", callback_data="show_dt"))

    local_time = mobs.WhichEpico.MR.as_timezone(query.data)
    local_time_fmt = local_time.strftime("%A %d de %B de %Y a las %H:%M")

    await query.message.edit_text(local_time_fmt, reply_markup=kb_markup)


@dp.message_handler(Text(equals=["gemas", "gems"], ignore_case=True))
async def gemas(message: Message):
    gems = gems.GemsInfo()
    emo = emoji.emojize

    als = gems.count_gems("Alsius")
    ign = gems.count_gems("Ignis")
    syr = gems.count_gems("Syrtis")

    red = emo(":red_circle:")
    gre = emo(":green_circle:")
    blu = emo(":blue_circle:")

    text = dedent(
        f"""
    {gre} Syrtis: *{syr}*
    {blu} Alsius: *{als}*
    {red} Ignis:  *{ign}*
    """
    )

    await message.reply(text, parse_mode="Markdown")


if __name__ == "__main__":
    if bot:
        executor.start_polling(dp)
